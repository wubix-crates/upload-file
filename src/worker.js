export const waitForFile = () =>
  new Promise((resolve) => {
    self.addEventListener(`message`, function handler(event) {
      if (`custom` in event.data) {
        resolve(event.data.custom);
        self.removeEventListener(`message`, handler);
      }
    });
  });
