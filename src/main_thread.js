const input = document.createElement(`input`);
input.type = `file`;
input.style.position = `fixed`;
input.style.opacity = `0`;
input.style.top = input.style.left = input.style.width = input.style.height = `0`;

input.addEventListener(`change`, () => {
  input.remove();
  if (input.files.length !== 1) {
    return;
  }
  const file = input.files[0];
  const reader = new FileReader();
  reader.onload = () =>
    window.processes
      .get(pid)
      .postMessage({ custom: [...new Uint8Array(reader.result)] });
  reader.readAsArrayBuffer(file);
});
document.body.append(input);

window.addEventListener(`keydown`, function handler(event) {
  if (event.key === `Enter`) {
    input.click();
  } else {
    window.processes.get(pid).postMessage({ custom: null });
  }
  window.removeEventListener(`keydown`, handler);
});
