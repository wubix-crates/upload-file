use js_sys::Promise;
use librust::{
    env::args_os,
    fs::write,
    io::{stderr, stdin, Write},
    process,
};
use wasm_bindgen::prelude::*;
use wasm_bindgen_futures::JsFuture;

#[wasm_bindgen(module = "/src/worker.js")]
extern "C" {
    #[wasm_bindgen(js_name = "waitForFile")]
    fn wait_for_file() -> Promise;
}

const MAIN_THREAD_CODE: &str = include_str!("./main_thread.js");

#[wasm_bindgen]
pub async fn start() {
    let output = if let Some(path) = args_os().nth(1) {
        path
    } else {
        librust::eprintln!("upload-file: error: no output specified").await;
        return;
    };

    let file: JsFuture = wait_for_file().into();
    write(
        "/dev/main_thread",
        format!("const pid = {};{}", process::id().await, MAIN_THREAD_CODE),
    )
    .await
    .unwrap();

    librust::eprint!("Press Enter to open a file chooser dialog, or any other key to exit").await;
    stderr().flush().await.unwrap();
    // todo: instead of reading a new line, we should actually put the terminal
    // into raw mode and read one character
    stdin().read_line(&mut String::new()).await.unwrap(); // eat a new line

    let result: Option<Vec<u8>> = serde_wasm_bindgen::from_value(file.await.unwrap()).unwrap();

    if let Some(bytes) = result {
        write(output, bytes).await.unwrap();
    } else {
        process::exit(1);
    }
}
